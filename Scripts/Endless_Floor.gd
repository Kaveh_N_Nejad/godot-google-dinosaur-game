extends Node2D

export(PackedScene) var floor_segment

onready var pos_y = get_viewport().size.y
var segment_width

var segment_array = []

func _ready():
	set_up()

func set_up():
	segment_array = []
	add_segment(0)
	segment_width = segment_array[0].get_node("Sprite").texture.get_size().x
	while (get_last_segment_x_pos() < get_viewport().size.x + segment_width):
		add_segment(get_last_segment_x_pos() + segment_width)


func add_segment(pos_x):
	var new_segment = floor_segment.instance()
	new_segment.position = Vector2(pos_x, pos_y)
	segment_array.append(new_segment)
	new_segment.connect("off_screen", self, "_on_segment_off_screen")
	add_child(new_segment)


func get_last_segment_x_pos():
	return segment_array.back().position.x


func move_segment(segment):
	segment.position.x = get_last_segment_x_pos() + segment_width


func _on_segment_off_screen():
	var las = segment_array.pop_front()
	move_segment(las)
	segment_array.append(las)
