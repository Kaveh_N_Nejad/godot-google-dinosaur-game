extends StaticBody2D

signal off_screen

func _on_VisibilityNotifier2D_screen_exited():
	emit_signal("off_screen")
