extends KinematicBody2D

export(int) var speed
export(int) var gravity
export(int) var innitial_jump_speed
export(int) var max_fall_vellocity

var movement
var is_jumping = false
var y_movement = 0


func _ready():
	movement = Vector2(speed,0)


func _physics_process(delta):
	move(delta)


func move(delta):
	if is_jumping:
		jumping()

	if not is_on_floor() and not is_jumping:
		fall()

	movement = Vector2(speed, y_movement)
	move_and_slide(movement, Vector2(0, -1))


func _input(event):
	if event.is_action_pressed("jump") and is_on_floor():
		start_jump()


func start_jump():
	is_jumping = true
	y_movement = innitial_jump_speed


func jumping():
	movement.y = y_movement
	y_movement = move_toward(y_movement, 0, gravity)
	if y_movement == 0:
		is_jumping = false


func fall():
	y_movement += gravity
